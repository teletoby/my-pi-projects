# -*- coding: utf-8 -*- 

import re
import urllib3
from bs4 import BeautifulSoup
from datetime import datetime, date



class CSTauxGetter(object):
    
    CS_TAUX_URL = u'https://hyporechner.credit-suisse.com/fr/interest-rates.jsp'
    
    #<p class="abstract">Taux actuels applicables aux modèles hypothécaires du 07.02.2014</p>
    RE_DATE_VALIDITY = re.compile(r'.*\s(\d{1,2})\.(\d{1,2})\.(\d{4,4})')
    
    #<p class="text text15">Taux d'intérêt actuel (applicable aux nouvelles affaires): 2,85%</p>
    RE_TAUX_VARIABLE = re.compile(r'.*(\d{1,2},\d{2,3})%')
    
    #u"Taux d'int\xe9r\xeat actuel: 2,60%, + commission sur cr\xe9dits \xbc% par trimestre"
    RE_TAUX_CC = re.compile(r'.*(\d{1,2},\d{2,3})%')
    
    # taux fix / flex
    RE_ANS_FIX = re.compile(r'(\d{1,2})\s*ans')
    RE_MOIS_FLEX = re.compile(r'(\d{1,2})\s*mois')
    RE_TAUX = re.compile(r'(\d{1,2},\d{2,3})%')

    __CODE_VERSION__ = '0.1'
    __NUM_FIX = 28
    

    def __init__(self):
        self._cpool = urllib3.PoolManager()

    def _get_page(self):
        resp = self._cpool.request('GET', self.CS_TAUX_URL)
        return(resp.data)

    def _do_soup(self, markup):
        soup = BeautifulSoup(markup, 'html5lib')
        return(soup)

    def _extract_data(self, soup, taxobject):
        # this part is hard-coded for the static website content and is subject to change.

        # general
        taxobject.update = datetime.today()
        taxobject.code_version = self.__CODE_VERSION__
        taxobject.source = self.CS_TAUX_URL

        # get validity date
        (day, month, year) = map(int, re.match(self.RE_DATE_VALIDITY, (soup.find_all('h1')[1]).find_next('p').string).groups())

        taxobject.validity = date(year, month, day) 
        # get taux
        headers = soup.find_all('h2')
        #print headers
        
        # variable
        taxobject.var = float((re.match(self.RE_TAUX_VARIABLE, headers[0].find_next('p').string).groups()[0]).replace(',', '.'))
        # fix
        tab_fix = headers[1].find_all_next('td')
        i = 0
        while (i < (len(tab_fix)-1)) and (i < self.__NUM_FIX):
            dur = self.RE_ANS_FIX.match((tab_fix[i]).string).groups()[0]
            taux = (self.RE_TAUX.match((tab_fix[i+1]).string).groups()[0]).replace(',', '.')
            print "i: ", i, " dur: ", dur, " taux: ", taux
            taxobject.__setattr__("fix_{0:s}a".format(dur), float(taux))
            i += 2
        # while
        # flex roll over
        tab_flex = headers[2].find_all_next('td')
        i = 0
        while i < (len(tab_flex)-1):
            dur = self.RE_MOIS_FLEX.match((tab_flex[i]).string).groups()[0]
            taux = (self.RE_TAUX.match((tab_flex[i+1]).string).groups()[0]).replace(',', '.')
            print "i: ", i, " dur: ", dur, " taux: ", taux
            taxobject.__setattr__("flex_{0:s}m".format(dur), float(taux))
            i += 2
        # while
        
        # cc
        taxobject.cc = float((re.match(self.RE_TAUX_CC, headers[3].find_next('p').string).groups()[0]).replace(',', '.'))
        return

# EOF