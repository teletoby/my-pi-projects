# -*- coding: utf-8 -*- 

import sys

from datetime import datetime

from mapping import CSTaux, create_engine, sessionmaker
from fetch_data_cs import CSTauxGetter

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.serializer import loads, dumps

from taux_getter import App



if __name__ == '__main__':

    App.check_paths()

    engine = create_engine('sqlite:///{0}'.format(App.__DB_PATH__), echo=False)
    Session = sessionmaker(bind=engine)
    session = Session()

    resp = session.query(CSTaux).all()

    fix = [c[0] for c in resp]
    flex = [c[1] for c in resp]
    validity = [c[2] for c in resp]




# EOF