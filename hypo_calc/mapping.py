# -*- coding: utf-8 -*- 

'''
Created on Feb 9, 2014

@author: tobi
'''

from sqlalchemy import MetaData, Table, Column, \
    Integer, String, DateTime, Date, Numeric, ForeignKey, and_, \
    Column, Integer, String, Numeric, Unicode, UniqueConstraint, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


Base = declarative_base()


class CSTaux (Base):
    
    __tablename__ = 'cs_taux'
    __table_args__ = (
        #UniqueConstraint('validity'),
    )

    id = Column(Integer, primary_key=True)
    validity = Column(Date, nullable=False)
    update = Column(DateTime, nullable=False) 
    source = Column(Unicode, nullable=False)
    code_version = Column(String, nullable=False)
    site_version = Column(String)
    var = Column(Float, nullable=False)
    fix_2a = Column(Float, nullable=False)
    fix_3a = Column(Float, nullable=False)
    fix_4a = Column(Float, nullable=False)
    fix_5a = Column(Float, nullable=False)
    fix_6a = Column(Float, nullable=False)
    fix_7a = Column(Float, nullable=False)
    fix_8a = Column(Float, nullable=False)
    fix_9a = Column(Float, nullable=False)
    fix_10a = Column(Float, nullable=False)
    fix_11a = Column(Float, nullable=False)
    fix_12a = Column(Float, nullable=False)
    fix_13a = Column(Float, nullable=False)
    fix_14a = Column(Float, nullable=False)
    fix_15a = Column(Float, nullable=False)
    flex_3m = Column(Float, nullable=False)
    flex_6m = Column(Float, nullable=False)
    flex_12m = Column(Float, nullable=False)
    cc = Column(Float, nullable=False)


# EOF