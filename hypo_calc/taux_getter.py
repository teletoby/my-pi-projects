#!/usr/bin/env python
# -*- coding: utf-8 -*- 
'''
Created on Mar 29, 2014

@author: tobi
'''

import sys
import os
import time
import random

from mapping import CSTaux, create_engine, sessionmaker
from fetch_data_cs import CSTauxGetter

from subprocess import call
from daemon.runner import DaemonRunner

import logging
LOGGER_PATH = '/tmp/hypo_calc/hypo_calc.log'
logger = logging.getLogger("HypoCalcLog")
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
handler = logging.FileHandler(LOGGER_PATH)
handler.setFormatter(formatter)
logger.addHandler(handler)





class App(object):

    #__WORK_DIR__ = '/var/run/hypo_calc'
    __WORK_DIR__ = '/tmp/hypo_calc'
    __DB_FILE__ = 'taux.db'
    __DB_PATH__ = os.path.join(__WORK_DIR__, __DB_FILE__)
    __PID_FILE__ = 'hypo_calc.pid'
    __LOGGER_PATH__ = LOGGER_PATH
    __GSYNC_PATH__ = ['gsync', '-r']
    __GSYNC_FETCH__ = __GSYNC_PATH__ + ['drive://hypo_calc/taux.db', __WORK_DIR__]
    __GSYNC_PUSH__ = __GSYNC_PATH__ + [__DB_PATH__, 'drive://hypo_calc/']
    __FREQ__ = 24.0*60.0*60.0

    @classmethod
    def check_paths(cls):
        logpath = os.path.dirname(cls.__LOGGER_PATH__)
        if os.path.exists(logpath) == False:
            os.mkdir(logpath)
        if os.path.exists(cls.__WORK_DIR__) == False:
            os.mkdir(cls.__WORK_DIR__)
        # check if database file is there and if not get it from google drive
        # since is running in the foreground also allows to authenticate
        if not os.path.exists(cls.__DB_PATH__):
            cls.__fetch_db_file__()
        return


    @classmethod
    def __fetch_db_file__(cls):
        try:
            retcode = call(cls.__GSYNC_FETCH__, shell=False)
            if retcode < 0:
                logger.warn("Child was terminated by signal {0:d}".format(-retcode))
            else:
                logger.debug("Child returned {0:d}".format(retcode))
        except OSError, e:
            logger.error("Execution failed: {0}".format(e))
        if retcode == 0:
            return(True)
        return(False)


    @classmethod
    def __push_db_file__(cls):
        try:
            retcode = call(cls.__GSYNC_PUSH__, shell=False)
            if retcode < 0:
                logger.warn("Child was terminated by signal {0:d}".format(-retcode))
            else:
                logger.debug("Child returned {0:d}".format(retcode))
        except OSError, e:
            logger.error("Execution failed: {0}".format(e))
        if retcode == 0:
            return(True)
        return(False)


    def __init__(self):
        logger.debug("Entering constructor!")
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path =  os.path.join(self.__WORK_DIR__, self.__PID_FILE__)
        logger.debug("Pid file: {0}".format(self.pidfile_path))
        self.pidfile_timeout = 5
        self._engine = None
        self._session = None
        random.seed()
        return


    def __del__(self):
        if self._session is not None:
            logger.info("Cleaning-up session!")
            self._session.close_all()
            self._session = None
            self._engine = None
            App.__push_db_file__()
        return


    def __check_db__(self):
        if self._engine is None:
            logger.debug("Creating engine!")
            engine = create_engine('sqlite:///{0}'.format(self.__DB_FILE__), echo=False)
            logger.debug("Creating tables!")
            CSTaux.metadata.create_all(bind=engine)
            self._engine = engine
        if self._session is None:
            logger.debug("Creating session!")
            Session = sessionmaker(bind=self._engine)
            self._session = Session()
        return


    def run(self):
        logger.debug("Entering run() function!")
        
        sleepinit = 0.0
        sleepend = self.__FREQ__
        while True:
            time.sleep(sleepinit)

            logger.debug("Ensuring that database stuff exists!")
            self.__check_db__()
            # create new row object
            taux = CSTaux()
            getter = CSTauxGetter()

            logger.debug("Getting data from website!")
            data = getter._get_page()
            logger.info("Got {0:d} bytes!".format(len(data)))
            soup = getter._do_soup(data)
            getter._extract_data(soup, taux)

            self._session.add(taux)
            self._session.commit()
            App.__push_db_file__()
            time.sleep(sleepend)
            
            rnd = random.random()
            sleepinit = (1.0-rnd)*self.__FREQ__
            sleepend = rnd*self.__FREQ__
            logger.debug("sleepinit: {0:f} sleepend: {1:f}".format(sleepinit, sleepend))

        # while
        return





if __name__ == "__main__":

    # verify that all the paths exist
    App.check_paths()
    app = App()    
    daemon_runner = DaemonRunner(app)
    #This ensures that the logger file handle does not get closed during daemonization
    daemon_runner.daemon_context.files_preserve=[handler.stream]
    daemon_runner.do_action()


# EOF